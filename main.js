const root = document.querySelector("#root");
const url = "https://ajax.test-danit.com/api/json/";
const loadingRoller = document.querySelector(".lds-roller");
const cardFragment = document.querySelector(".card-template").content;
const formFragment = document.querySelector(".form-template").content;
const buttons = [document.querySelector(".btn-add-card"), document.getElementsByClassName("card__btn-delete"), document.getElementsByClassName("btn-edit")];

class Requests {
    constructor(url) {
        this.url = url;
    }

    requestGet(entity = "", id = "") {
        return fetch(`${this.url}${entity}/${id}`, {
            method: "GET",
            headers: {
                "content-type": "application/json"
            },
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error(response.status);
                }
                return response.json();
            })
    }

    requestDelete(entity = "", id = "") {
        return fetch(`${this.url}${entity}/${id}`, {
            method: "DELETE",
            headers: {
                "content-type": "application/json"
            },
        }).then((response) => {
            if (!response.ok) {
                throw new Error(response.status);
            }
        })
    }

    requestPost(data, entity = "") {
        return fetch(`${this.url}${entity}`, {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: data,
        }).then((response) => {
            if (!response.ok) {
                throw new Error(response.status);
            }
        })
    }

    requestPut(data, entity = "", id = "") {
        return fetch(`${this.url}${entity}/${id}`, {
            method: "PUT",
            headers: {
                "content-type": "application/json"
            },
            body: data,
        }).then((response) => {
            if (!response.ok) {
                throw new Error(response.status);
            }
        })
    }
}

const requests = new Requests(url);

class Cards {
    constructor(url, root, cardFragment, formFragment) {
        this.url = url;
        this.root = root;
        this.cardFragment = cardFragment;
        this.formFragment = formFragment;
    }

    getUsersPosts() {
        return new Promise((resolve, reject) => {
            const arr = [];
            requests.requestGet("users")
                .then(usersData => {
                    arr.push(usersData);
                    return requests.requestGet("posts");
                })
                .then((postsData) => {
                    arr.push(postsData);
                    resolve(arr);
                })
                .catch(e => {
                    reject(e)
                });
        })

    }

    renderCards() {
        this.getUsersPosts()
            .then((usersPostsArr) => {
                this.users = usersPostsArr[0];
                this.posts = usersPostsArr[1];
                const postsWithUsers = this.posts.map(({id: postsId, userId, body, title}) => {
                    const newObj = {};
                    usersPostsArr[0].forEach(({id, name, email}) => {
                        if (userId === id) {
                            newObj.id = postsId;
                            newObj.name = name;
                            newObj.email = email;
                            newObj.title = title;
                            newObj.body = body;
                        }
                    })
                    return newObj;
                })

                this.cardTemplate = this.cardFragment.querySelector(".card");
                this.postsToShow = postsWithUsers.map((el) => {
                    const card = this.cardTemplate.cloneNode(true);
                    const cardTitle = card.querySelector(".card__title");
                    const cardText = card.querySelector(".card__text");
                    const userName = card.querySelector(".user__name");
                    const userMail = card.querySelector(".user__mail");
                    card.setAttribute(`data-id`, el.id);
                    cardTitle.textContent = el.title;
                    cardText.textContent = el.body;
                    userName.textContent = el.name;
                    userMail.textContent = el.email;
                    return card;
                })
                this.root.append(...this.postsToShow);
                loadingRoller.classList.add("hidden");
            })
            .catch(e => {
                console.log(e.message)
            })
    }

    renderCard({title, body: text, userId}) {
        const newCard = this.cardTemplate.cloneNode(true);
        const cardTitle = newCard.querySelector(".card__title");
        const cardText = newCard.querySelector(".card__text");
        const userName = newCard.querySelector(".user__name");
        const userMail = newCard.querySelector(".user__mail");
        cardTitle.textContent = title;
        cardText.textContent = text;
        const user = this.users.filter(({id}) => userId === id);
        const {name, email} = user[0];
        userName.textContent = name;
        userMail.textContent = email;
        newCard.setAttribute(`data-id`, 1);
        this.postsToShow.forEach((el) => {
            el.setAttribute("data-id", `${++el.dataset.id}`);
        });
        this.postsToShow.unshift(newCard);
        root.prepend(newCard);
    }

    deleteCard(btn) {
        const currentCard = btn.parentElement.parentElement;
        const currentCardId = currentCard.dataset.id;
        requests.requestDelete("posts", currentCardId)
            .then(() => {
                this.posts = this.posts.filter(({id}) => +id !== +currentCardId);
                let idCounter = 1;
                this.posts.forEach((value) => {
                    value.id = idCounter;
                    idCounter++;
                })
                idCounter = 1;
                this.postsToShow = this.postsToShow.filter((value) => {
                    return +value.dataset.id !== +currentCardId;
                });
                this.postsToShow.forEach((el) => {
                    el.setAttribute("data-id", idCounter);
                    idCounter++;
                });
                currentCard.remove();
            })
            .catch(e => {
                console.log(e.message)
            })
    }

    editCard(btn) {
        const currentCard = btn.parentElement.parentElement;
        if (btn.dataset.active) {
            this.removeEditForm(btn);
        } else {
            btn.setAttribute("data-active", "true");
            const currentCardId = currentCard.dataset.id;
            const title = currentCard.querySelector(".card__title");
            const text = currentCard.querySelector(".card__text");
            const formEditTemplate = document.querySelector(".form-edit-template").content;
            this.formEdit = formEditTemplate.querySelector(".form-edit").cloneNode(true);
            const formTitle = this.formEdit.querySelector("#title-edit");
            const formText = this.formEdit.querySelector("#text-edit");
            formTitle.value = title.textContent;
            formText.value = text.textContent;
            const openForms = this.root.querySelectorAll(".form-edit");
            [...openForms].forEach(form => {
                const btn = form.parentElement.querySelector(".btn-edit");
                this.removeEditForm(btn, form);
            })
            currentCard.append(this.formEdit);
            this.formEdit.addEventListener("submit", (e) => {
                e.preventDefault();
                if (formText.value.trim() !== "") {
                    const currentUser = this.posts.filter(({id, userId}) => {
                        if (+id === +currentCardId) {
                            return userId;
                        }
                    })
                    const editPost = {
                        id: currentCardId,
                        userId: currentUser[0].userId,
                        title: formTitle.value,
                        body: formText.value,
                    }
                    const editPostJson = JSON.stringify(editPost);
                    requests.requestPut(editPostJson, "posts", currentCardId)
                        .then(() => {
                            title.textContent = formTitle.value;
                            text.textContent = formText.value;
                            this.posts.forEach((value) => {
                                if (+value.id === +currentCardId) {
                                    value.title = formTitle.value;
                                    value.body = formText.value;
                                }
                            });
                            this.removeEditForm(btn);
                        })
                        .catch(e => {
                            console.log(e.message);
                        })
                } else {
                    alert("Enter text");
                }
            })
        }
    }

    removeEditForm(btn, form = this.formEdit) {
        form.remove();
        btn.removeAttribute("data-active", "true");
    }

    renderForm(buttonToDisable) {
        const formTemplate = this.formFragment.querySelector(".form-wrapper");
        document.body.classList.add("body-hidden");
        this.formWrapper = formTemplate.cloneNode(true);
        const form = this.formWrapper.querySelector(".form");
        const title = form.querySelector("#title");
        const text = form.querySelector("#text");
        root.append(this.formWrapper);
        form.addEventListener("submit", (e) => {
            e.preventDefault();
            if (text.value.trim() !== "") {
                const newPost = {
                    id: 1,
                    userId: 1,
                    title: title.value,
                    body: text.value,
                }
                const newPostJson = JSON.stringify(newPost);
                requests.requestPost(newPostJson, "posts")
                    .then(() => {
                        this.posts.forEach(({id}) => {
                            id++;
                        });

                        this.posts.unshift(newPost);
                        this.deleteForm();
                        buttonToDisable.forEach((value) => {
                            value.removeAttribute("disabled");
                        });
                        this.renderCard(newPost);
                    })
                    .catch(e => {
                        console.log(e.message);
                    })
            } else {
                alert("Enter text");
            }
        })
    }

    deleteForm() {
        this.formWrapper.remove();
        document.body.classList.remove("body-hidden");
    }
}

const cards = new Cards(url, root, cardFragment, formFragment);
cards.renderCards();

document.addEventListener("click", (e) => {
    if (e.target.closest("a")) {
        e.preventDefault();
    }
    if (e.target.closest(".card__btn-delete")) {
        if (e.target.className.includes("btn-delete__line")) {
            cards.deleteCard(e.target.parentElement);
        } else {
            cards.deleteCard(e.target);
        }
    }
    if (e.target.closest(".btn-add-card")) {
        this.buttonToDisable = [buttons[0], ...buttons[1], ...buttons[2]];
        this.buttonToDisable.forEach((value) => {
            value.setAttribute("disabled", "");
        })
        cards.renderForm(this.buttonToDisable);
    }
    if (e.target.closest(".btn-delete--form")) {
        this.buttonToDisable.forEach((value) => {
            value.removeAttribute("disabled");
        })
        cards.deleteForm();
    }
    if (e.target.closest(".btn-edit")) {
        let currentTarget = e.target;

        while (currentTarget.className !== "btn-edit") {
            currentTarget = currentTarget.parentElement;
        }
        cards.editCard(currentTarget);
    }
})